import { FieldValidator } from "final-form";
import React, { FocusEventHandler, ReactElement, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { Select as UISelect, SelectProps as UISelectProps } from "../../../ui/Select";

export type ExtraSelectProps<
    T,
    Multiple extends boolean | undefined,
    DisableClearable extends boolean | undefined,
    FreeSolo extends boolean | undefined,
> = {
    name: string;
    required?: boolean;
    requiredErrorMessage?: ReactNode;
    onChange?: UISelectProps<T, Multiple, DisableClearable, FreeSolo>["onChange"];
};

export type SelectProps<
    T,
    Multiple extends boolean | undefined,
    DisableClearable extends boolean | undefined,
    FreeSolo extends boolean | undefined,
> = Omit<UISelectProps<T, Multiple, DisableClearable, FreeSolo>, "onChange" | "value" | "name"> &
    ExtraSelectProps<T, Multiple, DisableClearable, FreeSolo>;

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<any> =>
    (value: string) =>
        value ? undefined : errorMessage;

/**
 * @deprecated
 */
export const SelectDeprecated = <
    T,
    Multiple extends boolean | undefined,
    DisableClearable extends boolean | undefined,
    FreeSolo extends boolean | undefined,
>(
    props: SelectProps<T, Multiple, DisableClearable, FreeSolo>,
): ReactElement => {
    const {
        label,
        name,
        onBlur,
        onChange,
        onFocus,
        options,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        TextFieldProps = {},
        ...selectProps
    } = props;

    const { input, meta } = useField<any>(name, {
        allowNull: true,
        defaultValue: null,
        validate: required ? requiredValidator(requiredErrorMessage) : undefined,
    });

    const onBlurHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onBlur(e);
            if (onBlur) {
                onBlur(e);
            }
        },
        [input, onBlur],
    );

    const onFocusHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onFocus(e);
            if (onFocus) {
                onFocus(e);
            }
        },
        [input, onFocus],
    );

    return (
        <UISelect
            {...selectProps}
            label={required ? `${label} *` : label}
            TextFieldProps={{
                ...TextFieldProps,
                helperText: meta.error && meta.touched ? meta.error : undefined,
                error: TextFieldProps.error || (meta.error && meta.touched),
            }}
            value={options.find((o: any) => o.value === input.value || o.id === input.value) as any}
            onChange={(event, value: any, reason, details) => {
                input.onChange(value?.id ?? value?.value ?? null);
                if (onChange) {
                    onChange(event, value, reason, details);
                }
            }}
            options={options}
            onBlur={onBlurHandler}
            onFocus={onFocusHandler}
        />
    );
};
