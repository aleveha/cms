import { ReactNode } from "react";

export interface LabelProps {
    className?: string;
    color?: "primary" | "secondary" | "error" | "warning" | "success" | "disabled";
    children: ReactNode;
    mt?: number;
    mb?: number;
    ml?: number;
    mr?: number;
    light?: boolean;
}
