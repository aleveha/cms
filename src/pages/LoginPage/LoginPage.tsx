import { NextPage } from "next";
import React, { useCallback } from "react";
import { login } from "../../api";
import { container } from "../../config";
import { LoginForm, LoginFormData, LoginFormProps } from "../../forms/LoginForm";
import { LoginLayout } from "../../lib/LoginLayout";
import { InitialProps, LoginPageConfig, LoginPageQueryParams } from "./types";

const INITIAL_VALUES: LoginFormData = {
    username: "",
    password: "",
};

export const LoginPage = (config: LoginPageConfig) => {
    const Component: NextPage<InitialProps> = (props) => {
        const onLogin = useCallback<LoginFormProps["onSubmit"]>(
            async (values) => {
                try {
                    const response = await login(null, values).then((r) => r.data);
                    await config.onLoginDone(response, props.redirectUrl);
                } catch (e) {
                    container.get("service.error").handleError(e);
                }
            },
            [props.redirectUrl],
        );

        return (
            <LoginLayout
                hideTitle={config.ui?.hideTitle}
                Logo={config.ui?.Logo}
                pageTitle={config.pageTitle}
                title={config.title}
            >
                <LoginForm
                    onSubmit={onLogin}
                    initialValues={INITIAL_VALUES}
                    onForgottenPassword={config.onForgottenPassword}
                />
            </LoginLayout>
        );
    };

    Component.getInitialProps = async (ctx) => {
        const query: LoginPageQueryParams = ctx.query;
        return {
            redirectUrl: query.redirect,
        };
    };

    return Component;
};
