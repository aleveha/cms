import { SvgIconProps } from "@mui/material/SvgIcon";
import { ComponentType, ReactElement } from "react";

export interface MenuItemProps<RouteList> {
    children?: ReactElement;
    icon?: ComponentType<SvgIconProps>;
    label: string;
    onClick?: (event?: any) => void;
    route?: keyof RouteList;
    params?: any; // TODO
}
