import { css } from "@emotion/react";
import { styled } from "@mui/material/styles";

export const LayoutRoot = styled("div")`
    display: flex;
`;

export const LayoutHeader = styled("div", { shouldForwardProp: (propName) => propName !== "hasHeader" })<{
    hasHeader?: boolean;
}>(
    ({ theme, hasHeader }) =>
        hasHeader &&
        css`
            background-color: ${theme.palette.primary.main};
            color: #fff;
        `,
);

export const LogoContainer: any = styled("div")`
    height: 100%;
    justify-content: center;
    width: 240px;
`;
