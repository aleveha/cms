import { ButtonProps as MuiButtonProps } from "@mui/material/Button";

export interface ButtonProps extends MuiButtonProps {
    loading?: boolean;
    mt?: number;
    mb?: number;
    ml?: number;
    mr?: number;
}
