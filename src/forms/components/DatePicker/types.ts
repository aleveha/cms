import { MomentFormatSpecification } from "moment";
import { DatePickerProps as UIDatePickerProps } from "../../../ui/DatePicker";
import { FieldValidator } from "final-form";

export interface DatePickerProps extends Omit<UIDatePickerProps, "onChange" | "value" | "renderInput"> {
    allowedFormats?: MomentFormatSpecification;
    name: string;
    requiredErrorMessage?: string;
    validate?: FieldValidator<string>[];
}
