import { ForgottenPasswordPage } from "../../src/pages/ForgottenPasswordPage";

const Page = ForgottenPasswordPage({
    title: "Zapomenuté heslo",
    onSubmit: (username) => {
        console.log(username);
        return Promise.resolve();
    },
});

export default Page;
