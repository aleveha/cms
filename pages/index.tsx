import { Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import axios from "axios";
import { NextPage } from "next";
import { useState } from "react";
import { menu } from "../config";
import { Layout } from "../src/lib/Layout";
import { FlashMessagesService } from "../src/services/FlashMessagesService";
import { Autocomplete } from "../src/ui/Autocomplete";
import { AutocompleteMultiple } from "../src/ui/AutocompleteMultiple";
import { CopyToClipboard } from "../src/ui/CopyToClipboard";
import { CopyToClipboardButton } from "../src/ui/CopyToClipboardButton";
import { FileInput } from "../src/ui/FileInput/FileInput";
import { Label, LabelProps } from "../src/ui/Label";
import { Paper } from "../src/ui/Paper";
import { Select } from "../src/ui/Select";
import { DatePicker } from "../src/ui/DatePicker";

const labelColors: Array<LabelProps["color"]> = ["primary", "secondary", "success", "warning", "error", "disabled"];

type Option = { id: number; label: string };

const smartAddressAxios = axios.create({
    baseURL: "https://www.smart-address.cz",
});

const loader = async (term: string): Promise<Array<Option>> => {
    const response = await smartAddressAxios.get("/api/v1/search", { params: { term, level: 1 } });
    return response.data.map((a: any) => ({ id: a.id, label: `${a.ulice}, ${a.obec}` }));
};

const options = [
    { id: 1, label: "Option 1" },
    { id: 2, label: "Option 2" },
    { id: 3, label: "Option 3" },
];

const Index: NextPage = () => {
    const [autocompleteValue, setAutocompleteValue] = useState<Option | null>(null);
    const [autocompleteMultipleValue, setAutocompleteMultipleValue] = useState<Option[]>([]);
    const [datePickerValue, setDatePickerValue] = useState<any>(null);
    const [fileInputValue, setFileInputValue] = useState<File | null | undefined>(null);

    return (
        <Layout title="UI komponenty" subtitle="Subtitle" menuConfiguration={menu} header={<>Header obsah</>}>
            <Paper>
                <Typography component="h2" variant="h4">
                    Autocomplete
                </Typography>
                <Box mt={2}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Autocomplete
                                label="Autocomplete"
                                loadOptions={loader}
                                value={autocompleteValue}
                                onChange={(_, value) => setAutocompleteValue(value)}
                            />
                        </Grid>
                    </Grid>
                </Box>
            </Paper>
            <Paper mt={2}>
                <Typography component="h2" variant="h4">
                    AutocompleteMultiple
                </Typography>
                <Box mt={2}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <AutocompleteMultiple
                                label="Autocomplete multiple"
                                loadOptions={loader}
                                value={autocompleteMultipleValue}
                                onChange={(_, value) => setAutocompleteMultipleValue(value)}
                                getOptionLabel={(o) => o.label}
                            />
                        </Grid>
                    </Grid>
                </Box>
            </Paper>
            <Paper mt={2}>
                <Typography component="h2" variant="h4">
                    CopyToClipboard
                </Typography>
                <Box style={{ marginTop: 16 }}>
                    <CopyToClipboard value="https://www.uxf.cz" label="Zkopírovat url adresu" />
                </Box>
            </Paper>
            <Paper mt={2}>
                <Typography component="h2" variant="h4">
                    ClipboardButton
                </Typography>
                <Box style={{ marginTop: 16 }}>
                    <CopyToClipboardButton textToCopy="Text ke zkopírování" />
                </Box>
            </Paper>
            <Paper mt={2}>
                <Typography component="h2" variant="h4">
                    DatePicker
                </Typography>
                <Box style={{ marginTop: 16 }}>
                    <DatePicker label="DatePicker" value={datePickerValue} onChange={setDatePickerValue} clearable />
                </Box>
            </Paper>
            <Paper mt={2}>
                <Typography component="h2" variant="h4">
                    FileInput
                </Typography>
                <Box style={{ marginTop: 16 }}>
                    <FileInput
                        id="file-input-name"
                        name="file-input"
                        value={fileInputValue}
                        onChange={setFileInputValue}
                        label="Profilová fotka"
                        placeholder="Vyberte soubor"
                    />
                </Box>
            </Paper>
            <Paper mt={2}>
                <Typography component="h2" variant="h4">
                    FlashMessage
                </Typography>
                <Box style={{ marginTop: 16 }}>
                    <Button onClick={() => FlashMessagesService.info("Info!")} variant="outlined">
                        Info
                    </Button>
                    <Button
                        onClick={() => FlashMessagesService.success("Success!")}
                        variant="outlined"
                        color="success"
                        sx={{ ml: 2 }}
                    >
                        Success
                    </Button>
                    <Button
                        onClick={() => FlashMessagesService.warning("Warning!")}
                        variant="outlined"
                        color="warning"
                        sx={{ ml: 2 }}
                    >
                        Warning
                    </Button>
                    <Button
                        onClick={() => FlashMessagesService.error("Error!")}
                        variant="outlined"
                        color="error"
                        sx={{ ml: 2 }}
                    >
                        Error
                    </Button>
                </Box>
            </Paper>
            <Paper mt={2}>
                <Typography component="h2" variant="h4">
                    Label
                </Typography>
                <Box>
                    {labelColors.map((c) => (
                        <Label key={c} color={c} mr={2} mt={2}>
                            {c}
                        </Label>
                    ))}
                </Box>
                <Box>
                    {labelColors.map((c) => (
                        <Label key={c} color={c} light mr={2} mt={2}>
                            {c}
                        </Label>
                    ))}
                </Box>
            </Paper>
            <Paper mt={2}>
                <Typography component="h2" variant="h4">
                    Select
                </Typography>
                <Box mt={2}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Select<any> label="Autocomplete" onChange={() => null} value={null} options={options} />
                        </Grid>
                    </Grid>
                </Box>
            </Paper>
        </Layout>
    );
};

export default Index;
