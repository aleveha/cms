export interface CmsContextType {
    sidebar: {
        isOpen: boolean;
        open: () => void;
        close: () => void;
    };
}
