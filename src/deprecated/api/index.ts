import { autocompleteRows as autocomplete } from "../../api";

/** @deprecated use import { autocompleteRows } from @uxf/cms/api; */
export const autocompleteRows = (entityAlias: string, fieldName: string, term = "") => {
    return autocomplete(null, { entityAlias, fieldName }, { term }).then((r) => r.data);
};
