import ExtendableError from "es6-error";

export class ForbiddenError extends ExtendableError {
    constructor(message = "Unauthorized.") {
        super(message);
    }
}
