import { TextFieldProps } from "@mui/material/TextField";
import { AutocompleteProps as MuiAutocompleteProps } from '@mui/material/Autocomplete';

interface ExtraAutocompleteProps<T> {
    placeholder?: TextFieldProps["placeholder"];
    label?: TextFieldProps["label"];
    loadOptions: (term: string) => Promise<T[]>;
    TextFieldProps?: Partial<TextFieldProps>;
}

export type AutocompleteProps<
    T,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined
> = Omit<MuiAutocompleteProps<T, false, DisableClearable, FreeSolo>, "options" | "renderInput" | "multiple"> &
    ExtraAutocompleteProps<T>;
