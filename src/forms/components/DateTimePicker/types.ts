import { DateTimePickerProps as MuiDateTimePickerProps } from "@mui/lab/DateTimePicker";
import { TextFieldProps } from "@mui/material";
import { FieldValidator } from "final-form";

export interface DateTimePickerProps
    extends Omit<MuiDateTimePickerProps, "name" | "required" | "onChange" | "value" | "renderInput"> {
    error?: boolean;
    name: string;
    required?: boolean;
    requiredErrorMessage?: string;
    validate?: FieldValidator<string>[];
    variant?: TextFieldProps["variant"];
}
