import React from "react";

export interface CopyToClipboardProps {
    value: string;
    disabled?: boolean;
    label?: React.ReactNode;
}
