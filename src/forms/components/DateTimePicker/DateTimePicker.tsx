import MuiDateTimePicker, { DateTimePickerProps as MuiDateTimePickerProps } from "@mui/lab/DateTimePicker";
import { TextField } from "@mui/material";
import { FieldValidator } from "final-form";
import { Moment } from "moment";
import React, { ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { composeValidators } from "../../../utils/composeValidators";
import { DateTimePickerProps } from "./types";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<string> =>
    (value: string) =>
        value ? undefined : errorMessage;

export const DateTimePicker: React.FC<DateTimePickerProps> = (props) => {
    const {
        disabled,
        error,
        label,
        name,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        validate = [],
        variant = "outlined",
        // TODO rest props
    } = props;

    const validator = composeValidators(
        required && !disabled ? requiredValidator(requiredErrorMessage) : undefined,
        ...validate,
    );

    const { input, meta } = useField<string | null>(name, {
        allowNull: true,
        validate: validator,
    });

    const onChange = useCallback<MuiDateTimePickerProps<Moment>["onChange"]>(
        (date) => input.onChange(date?.format("YYYY-MM-DD HH:mm")),
        [input],
    );

    return (
        <MuiDateTimePicker
            clearable
            renderInput={(props) => (
                <TextField
                    {...props}
                    variant={variant}
                    size="small"
                    helperText={meta.touched && meta.error}
                    error={error || !!(meta.touched && meta.error)}
                    onFocus={input.onFocus}
                    onBlur={input.onBlur}
                    fullWidth
                />
            )}
            ampm={false}
            label={
                <>
                    {label}
                    {required ? " *" : ""}
                </>
            }
            disabled={disabled}
            value={input.value || null}
            onChange={onChange}
            onClose={input.onBlur}
            onOpen={input.onFocus}
        />
    );
};
