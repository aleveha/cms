import Grid from "@mui/material/Grid";
import { FieldValidator } from "final-form";
import React, { useEffect, useState } from "react";
import { Form } from "react-final-form";
import { Button } from "../../ui/Button";
import { TextInput } from "../components/TextInput";
import { RenewPasswordFormData, RenewPasswordFormProps } from "./types";

const passwordsAreSame: FieldValidator<string> = (_, allValues) => {
    const { password, passwordAgain } = allValues as RenewPasswordFormData;
    return password === passwordAgain ? null : "Hesla musí být stejná";
};

export const RenewPasswordForm: React.FC<RenewPasswordFormProps> = (props) => {
    const [formDisabled, setFormDisabled] = useState(true);

    useEffect(() => {
        setFormDisabled(false);
    }, []);

    return (
        <Form {...props}>
            {({ handleSubmit, submitting }) => (
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextInput
                                disabled={formDisabled}
                                label="Nové heslo"
                                name="password"
                                type="password"
                                required
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextInput
                                disabled={formDisabled}
                                label="Znovu nové heslo"
                                name="passwordAgain"
                                type="password"
                                required
                                validate={[passwordsAreSame]}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button
                                color="primary"
                                disabled={submitting}
                                fullWidth
                                loading={submitting}
                                type="submit"
                                variant="contained"
                                mt={2}
                            >
                                Změnit heslo
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            )}
        </Form>
    );
};
