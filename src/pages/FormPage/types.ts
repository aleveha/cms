import { EntityMetaSchema, FieldSchemaResponse, FormSchemaResponse } from "../../api";
import React, { ReactElement } from "react";

export interface FieldProps {
    fieldSchema: FieldSchemaResponse;
    entityAlias: string;
    isEditing?: boolean;
    prefix?: string;
}

export interface FormPageConfig<T = { id?: number | null }> {
    allowedRoles?: string[];
    configureSchema?: (schema: FormSchemaResponse) => FormSchemaResponse;
    entityAlias?: string | ((ctx: any) => string);
    entityId?: number | ((ctx: any) => number);
    onLoad?: (ctx: any, entityAlias: string, id: number) => Promise<T>;
    onSave?: (data: T, entityAlias: string) => Promise<T>;
    onSaveDone?: (data: T, entityAlias: string) => void;
    ui?: {
        Field?: React.FC<FieldProps>;
        Layout?: ReactElement | ((entityMetaSchema: EntityMetaSchema) => ReactElement);
    };
}

export interface FormPageQueryParams {
    id?: string;
    entityAlias: string;
}
