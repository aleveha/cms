import { Schema } from "@uxf/data-grid";
import { EntityMetaSchema } from "../../../api";
import { IReduxState } from "../types";
import { EntityFormSchema } from "../../types/Form";

export const metaSchemaExistsSelector = (state: IReduxState): boolean => Object.keys(state.entities.meta).length > 0;

export const metaSchemaSelector =
    (entityAlias: string) =>
    (state: IReduxState): EntityMetaSchema =>
        state.entities.meta[entityAlias];

export const gridSchemaExistsSelector =
    (entityAlias: string) =>
    (state: IReduxState): boolean =>
        !!state.entities.grid[entityAlias];

export const gridSchemaSelector =
    (entityAlias: string) =>
    (state: IReduxState): Schema<any> =>
        state.entities.grid[entityAlias];

export const formSchemaExistsSelector =
    (entityAlias: string) =>
    (state: IReduxState): boolean =>
        !!state.entities.form[entityAlias];

export const formSchemaSelector =
    (entityAlias: string) =>
    (state: IReduxState): EntityFormSchema =>
        state.entities.form[entityAlias];
