import Link from "@mui/material/Link";
import ListItemIcon from "@mui/material/ListItemIcon";
import MuiMenuItem from "@mui/material/MenuItem";
import Typography from "@mui/material/Typography";
import React, { ReactElement } from "react";
import { container } from "../../config";
import { MenuItemProps } from "./types";

export const MenuItem = <T extends any>(props: MenuItemProps<T>): ReactElement => {
    const { children, icon, label, onClick, route, params } = props;
    const MenuIcon: any = icon ?? null; // TODO

    const Content = children ?? (
        <MuiMenuItem dense onClick={onClick} sx={{ py: 0, minHeight: 32 }}>
            {!!icon && (
                <ListItemIcon sx={{ p: 0, minWidth: 32 }}>
                    <MenuIcon />
                </ListItemIcon>
            )}
            <Typography variant="inherit" noWrap>
                {label}
            </Typography>
        </MuiMenuItem>
    );

    if (route) {
        const UxfLink = container.get("router").getLink();
        return (
            <UxfLink route={route} params={params} passHref>
                <Link color="inherit" underline="none">
                    {Content}
                </Link>
            </UxfLink>
        );
    }

    return Content;
};
