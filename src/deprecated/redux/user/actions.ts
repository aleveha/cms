import UserResponse from "../../../api";
import { ActionsUnion, createAction } from "../reduxHelper";

export const USER_LOGOUT = "user/LOGOUT";
export const USER_PERSIST = "user/PERSIST";

export const onUserPersist = (user: UserResponse) => createAction(USER_PERSIST, { user });

export const onUserLogout = () => createAction(USER_LOGOUT);

export interface IState {
    user?: UserResponse;
    loginLoading?: boolean; // TODO: use createApiAction
}

export const Actions = {
    onUserPersist,
    onUserLogout,
};

export type ActionTypes = ActionsUnion<typeof Actions>;
