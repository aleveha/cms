import { TextFieldProps } from "@mui/material/TextField";
import { AutocompleteProps as MuiAutocompleteProps } from '@mui/material/Autocomplete';

interface ExtraAutocompleteMultipleProps<T> {
    placeholder?: TextFieldProps["placeholder"];
    label?: TextFieldProps["label"];
    loadOptions: (term: string) => Promise<T[]>;
    TextFieldProps?: Partial<TextFieldProps>;
}

export type AutocompleteMultipleProps<
    T,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined
> = Omit<MuiAutocompleteProps<T, true, DisableClearable, FreeSolo>, "options" | "renderInput" | "multiple"> &
    ExtraAutocompleteMultipleProps<T>;
