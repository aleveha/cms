import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
    palette: {
        primary: {
            main: "#5d486e",
        },
        secondary: {
            main: "#45cedb",
        },
        error: {
            main: "#ed1c24",
        },
        background: {
            default: "#f5f5f5",
        },
    },
    typography: {
        fontFamily: [
            "LatoCustom",
            "-apple-system",
            "BlinkMacSystemFont",
            '"Segoe UI"',
            "Roboto",
            '"Helvetica Neue"',
            "Arial",
            "sans-serif",
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(","),
        h5: {
            fontWeight: "bold",
        },
    },
});
