import React, { FocusEventHandler, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { FieldValidator } from "final-form";
import { composeValidators } from "../../../utils/composeValidators";
import { NumberInputProps } from "./types";
import { NumberInput as UINumberInput } from "../../../ui/NumberInput";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<string> =>
    (value: string) =>
        value === "" || value === null || value === undefined ? errorMessage : undefined;

export const NumberInput: React.FC<NumberInputProps> = (props) => {
    const {
        disabled,
        error,
        label,
        name,
        onBlur,
        onFocus,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        validate = [],
        ...restProps
    } = props;

    const validator = composeValidators(
        required && !disabled ? requiredValidator(requiredErrorMessage) : undefined,
        ...validate,
    );

    const { input, meta } = useField<string>(name, { validate: validator });

    const onBlurHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onBlur(e);
            if (onBlur) {
                onBlur(e);
            }
        },
        [input, onBlur],
    );

    const onFocusHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onFocus(e);
            if (onFocus) {
                onFocus(e);
            }
        },
        [input, onFocus],
    );

    return (
        <UINumberInput
            error={error || !!(meta.touched && meta.error)}
            helperText={meta.touched && meta.error}
            disabled={disabled}
            fullWidth
            label={
                <>
                    {label}
                    {required ? " *" : ""}
                </>
            }
            name={name}
            onChange={input.onChange}
            value={input.value}
            onBlur={onBlurHandler}
            onFocus={onFocusHandler}
            {...restProps}
        />
    );
};
