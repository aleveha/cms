import UserResponse from "../../../api";
import { IReduxState } from "../types";

export const getLoggedUser = (state: IReduxState): UserResponse | null => state.user.user || null;
