import { DatePickerProps as MuiDatePickerProps } from "@mui/lab/DatePicker";
import { TextFieldProps } from "@mui/material";
import { Moment } from "moment";
import { ReactNode } from "react";

export interface DatePickerProps extends Omit<MuiDatePickerProps<Moment>, "renderInput"> {
    required?: boolean;
    error?: ReactNode;
    onFocus?: () => void;
    onBlur?: () => void;
    variant?: TextFieldProps["variant"];
}
