import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import React, { ReactElement } from "react";
import { container } from "../../config";
import { ListItemProps } from "./types";

export const ListItem = <T extends any>(props: ListItemProps<T>): ReactElement => {
    const { children, icon, label, onClick, onMouseEnter, onMouseLeave, route, params, level = 0, selected } = props;
    const MenuIcon: any = icon ?? null; // TODO

    const Content = children ?? (
        <ListItemButton
            dense
            onClick={onClick}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            selected={selected}
            sx={{ py: 0, minHeight: 32, px: 1 }}
        >
            {!!icon && level !== 1 && (
                <ListItemIcon sx={{ pl: level * 2, pr: 0, minWidth: 32 }}>
                    <MenuIcon />
                </ListItemIcon>
            )}
            <ListItemText
                primary={label}
                sx={{ ml: level > 0 ? 4 : undefined }}
                primaryTypographyProps={{ fontSize: 14 }}
            />
        </ListItemButton>
    );

    if (route) {
        const UxfLink = container.get("router").getLink();
        return (
            <UxfLink route={route} params={params} passHref>
                {Content}
            </UxfLink>
        );
    }

    return Content;
};
