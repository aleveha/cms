import Add from "@mui/icons-material/Add";
import Box from "@mui/material/Box";
import Fab from "@mui/material/Fab";
import Tooltip from "@mui/material/Tooltip";
import { DataGrid, Loader, Request, Response } from "@uxf/data-grid";
import { queryParamToString } from "@uxf/router";
import { NextPage } from "next";
import Router from "next/router";
import qs from "qs";
import React, { useCallback } from "react";
import { parse as parseUrl } from "url";
import { userConfigGet } from "../../api";
import { withAuthenticate } from "../../deprecated/hoc/withAuthenticate";
import { createAxiosInstance } from "../../lib/api";
import { loadEntityGridSchema, loadEntityMetaSchema } from "./getInitialPropsHelper";
import { GridPageComponentProps, GridPageConfig, LayoutComponent } from "./types";
import { useHiddenColumns } from "./useHiddenColumns";

const { axiosRequest } = createAxiosInstance();

export const dataGridLoader = (gridName: string, request: Request): Promise<Response> => {
    return axiosRequest<Response>(null, `/api/cms/datagrid/${gridName}`, "get", null, request).then((r) => r.data);
};

const DefaultLayout: LayoutComponent = (props) => <div {...props} />;

const GridPageComponent: React.FC<GridPageComponentProps> = (props) => {
    const {
        metaSchema,
        entityAlias,
        gridSchema,
        getOpenUrl,
        getEditUrl,
        onOpen,
        onAdd,
        onEdit,
        onRemove,
        BodyCell,
        FilterInput,
        query,
        initialHiddenColumns,
        loader,
        title,
        ActionCell,
        FilterListItem,
        Layout = DefaultLayout,
        actionCellWidth,
        rowHeight,
        FullTextInputProps,
        defaultConfig,
    } = props;
    const hiddenColumnProps = useHiddenColumns(entityAlias, initialHiddenColumns);

    const universalLoader = useCallback<Loader>(
        (gridName, request, encodedRequest) => {
            const response = dataGridLoader(gridName ?? "", request);

            // save filter to url
            const newQuery = { ...query, filter: encodedRequest };

            // TODO @vejvis - dodělat vlastní router a tuhle magii smazat!
            Router.replace(
                `${parseUrl(window.history.state?.url || "").pathname}?${qs.stringify(newQuery)}`,
                `${parseUrl(window.history.state?.as || "").pathname}?${qs.stringify(newQuery)}`,
                { shallow: true },
            );

            return response;
        },
        [query],
    );

    return (
        <Layout key={entityAlias} title={title ?? metaSchema?.title ?? ""}>
            <Box mb={10}>
                <DataGrid
                    schema={gridSchema}
                    initialState={query?.filter ? queryParamToString(query?.filter) : undefined}
                    title={title ?? metaSchema?.title}
                    onCsvDownload={(r) => window.open(`/api/cms/datagrid/export/${entityAlias}?${qs.stringify(r)}`)}
                    gridName={entityAlias}
                    loader={loader ?? universalLoader}
                    BodyCell={BodyCell}
                    FilterInput={FilterInput}
                    FilterListItem={FilterListItem}
                    {...hiddenColumnProps}
                    onOpen={onOpen && ((row) => onOpen(entityAlias, row))}
                    getOpenUrl={getOpenUrl && ((row) => getOpenUrl(entityAlias, row))}
                    onEdit={onEdit && ((row) => onEdit(entityAlias, row))}
                    getEditUrl={getEditUrl && ((row) => getEditUrl(entityAlias, row))}
                    onRemove={onRemove && ((row) => onRemove(entityAlias, row))}
                    ActionCell={ActionCell}
                    actionCellWidth={actionCellWidth as any}
                    rowHeight={rowHeight}
                    FullTextInputProps={FullTextInputProps}
                    defaultConfig={defaultConfig}
                />
            </Box>
            {onAdd && (!metaSchema || metaSchema?.actions.includes("add")) && (
                <Tooltip title="Přidat záznam">
                    <Fab
                        sx={{ position: "fixed", bottom: 16, right: 16 }}
                        onClick={() => onAdd(entityAlias)}
                        color="primary"
                    >
                        <Add />
                    </Fab>
                </Tooltip>
            )}
        </Layout>
    );
};

GridPageComponent.displayName = "GridPageComponent";

export const GridPage = (config: GridPageConfig) => {
    const Component: NextPage<any> = (props) => {
        return (
            <GridPageComponent
                {...props}
                title={config.title}
                loader={config.loader}
                onEdit={config.onEdit}
                getEditUrl={config.getEditUrl}
                onAdd={config.onAdd}
                onRemove={config.onRemove}
                onOpen={config.onOpen}
                getOpenUrl={config.getOpenUrl}
                BodyCell={config.BodyCell}
                ActionCell={config.ActionCell}
                FilterInput={config.FilterInput}
                FilterListItem={config.FilterListItem}
                actionCellWidth={config.actionCellWidth}
                Layout={config.Layout}
                rowHeight={config.rowHeight}
                FullTextInputProps={config.FullTextInputProps}
                defaultConfig={config.defaultConfig}
            />
        );
    };

    Component.getInitialProps = async (ctx) => {
        const entityAlias = typeof config.entityAlias === "function" ? config.entityAlias(ctx) : config.entityAlias;
        const initialHiddenColumns = await userConfigGet<string[]>(ctx, `v1--hidden-columns--${entityAlias}`, []);

        return {
            entityAlias,
            initialHiddenColumns,
            query: ctx.query,
            metaSchema: await loadEntityMetaSchema(ctx, entityAlias),
            gridSchema: await loadEntityGridSchema(ctx, entityAlias),
        };
    };

    return withAuthenticate({ allowedRoles: config.allowedRoles || ["ROLE_ROOT"] })(Component);
};
