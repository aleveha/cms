import { createStore, applyMiddleware, combineReducers, Store } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import entitiesReducer from "../redux/entities/reducer";
import userReducer from "../redux/user/reducer";
import { IReduxState } from "./types";

// TODO: create root reducer to have possibility reset store on logout...
export const initializeStore = (initialState: Partial<IReduxState> = {}): Store<IReduxState> =>
    createStore(
        combineReducers({
            entities: entitiesReducer,
            user: userReducer,
        }),
        initialState,
        composeWithDevTools(applyMiddleware()),
    );
