type ConfigStore = {
    "api-url": string;
    "app-name": string;
};

export class Config {
    private store: Partial<ConfigStore> = {
        "app-name": "UXFans",
    };

    public set<T extends keyof ConfigStore>(key: T, value: ConfigStore[T]): Config {
        this.store[key] = value;
        return this;
    }

    public get<T extends keyof ConfigStore>(key: T): ConfigStore[T] {
        const value = this.store[key];

        if (value === undefined) {
            throw new Error(`Config attribute '${key}' is empty.`);
        }

        return value as any;
    }
}
