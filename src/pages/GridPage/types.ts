import {
    ActionCellWithRequiredWidth,
    BodyCellComponent,
    DataGridProps,
    FilterInputComponent,
    FilterListItemComponent,
    Loader,
    Schema,
} from "@uxf/data-grid";
import { NextPageContext } from "next";
import { ParsedUrlQuery } from "querystring";
import { EntityMetaSchema } from "../../api";
import { LayoutProps } from "../../lib/Layout";

export type LayoutComponent = React.FC<LayoutProps>;

export type GridPageComponentProps = {
    entityAlias: string;
    title?: string;
    initialHiddenColumns: string[];
    onOpen?: (entityAlias: string, row: any) => void;
    getOpenUrl?: (entityAlias: string, row: any) => string | null | undefined;
    onAdd?: (entityAlias: string) => void;
    onEdit?: (entityAlias: string, row: any) => void;
    getEditUrl?: (entityAlias: string, row: any) => string | null | undefined;
    onRemove?: (entityAlias: string, row: any) => void;
    query: ParsedUrlQuery;
    loader?: Loader;
    gridSchema: Schema<any>;
    metaSchema?: EntityMetaSchema;
    BodyCell?: BodyCellComponent<any, any>;
    FilterInput?: FilterInputComponent<any>;
    FilterListItem?: FilterListItemComponent<any>;
    Layout?: LayoutComponent;
    rowHeight?: DataGridProps<any, any>["rowHeight"];
    FullTextInputProps?: DataGridProps<any, any>["FullTextInputProps"];
    defaultConfig?: DataGridProps<any, any>["defaultConfig"];
} & ActionCellWithRequiredWidth<any>;

export type EntityAliasExtractor = (ctx: NextPageContext) => string;

export type GridPageConfig<T extends object = any> = {
    title?: string;
    entityAlias: string | EntityAliasExtractor;
    onOpen?: (entityAlias: string, row: T) => void;
    getOpenUrl?: (entityAlias: string, row: T) => string | undefined | null;
    onAdd?: (entityAlias: string) => void;
    onEdit?: (entityAlias: string, row: T) => void;
    getEditUrl?: (entityAlias: string, row: T) => string | undefined | null;
    onRemove?: (entityAlias: string, row: T) => void;
    allowedRoles?: string[];
    loader?: Loader;
    BodyCell?: BodyCellComponent<any, any>;
    FilterInput?: FilterInputComponent<any>;
    FilterListItem?: FilterListItemComponent<any>;
    Layout?: LayoutComponent;
    rowHeight?: DataGridProps<any, any>["rowHeight"];
    FullTextInputProps?: DataGridProps<any, any>["FullTextInputProps"];
    defaultConfig?: DataGridProps<any, any>["defaultConfig"];
} & ActionCellWithRequiredWidth<any>;
