import React, { useCallback } from "react";
import TextField, { TextFieldProps } from "@mui/material/TextField";
import { NumberInputProps } from "./types";

export const getRegExp = (decimalPlaces?: number) => {
    // integer
    if (decimalPlaces === 0) {
        return /^(\+|-)?\d*$/;
    }

    // float
    if (decimalPlaces === undefined) {
        return /^(\+|-)?(\d+(\.|,)?\d*)?$/;
    }

    // float with decimal places
    return new RegExp(`^(\\+|-)?(\\d+(\\.|,)?\\d{0,${decimalPlaces}})$`);
};

export const NumberInput: React.FC<NumberInputProps> = (props) => {
    const { onChange, decimalPlaces, value, ...inputProps } = props;

    const onChangeHandler = useCallback<NonNullable<TextFieldProps["onChange"]>>(
        (e) => {
            const value = e.target.value.replace(",", ".").replace(/\s/g, "");

            const regExp = getRegExp(decimalPlaces);

            if (value !== "" && !regExp.test(value)) {
                return;
            }

            if (onChange) {
                onChange(value, e);
            }
        },
        [onChange, decimalPlaces],
    );

    const normalizedValue = (value || "").replace(".", ",").replace(/\B(?=(\d{3})+(?!\d))/g, " ");

    return (
        <TextField
            inputMode="numeric"
            variant="outlined"
            size="small"
            {...inputProps}
            onChange={onChangeHandler}
            value={normalizedValue}
        />
    );
};
