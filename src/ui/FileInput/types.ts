import { ReactNode } from "react";

export interface FileInputProps {
    id: string;
    name: string;
    placeholder?: string;
    label: ReactNode;
    onChange: (file: File | null) => void;
    value: File | null | undefined;
}
