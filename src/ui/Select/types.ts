import { AutocompleteProps } from "@mui/material/Autocomplete";
import { TextFieldProps } from "@mui/material/TextField";

export type SelectProps<
    T,
    Multiple extends boolean | undefined = undefined,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
> = {
    label?: string;
    TextFieldProps?: Partial<TextFieldProps>;
    onChange: NonNullable<AutocompleteProps<T, Multiple, DisableClearable, FreeSolo>["onChange"]>;
} & Omit<AutocompleteProps<T, Multiple, DisableClearable, FreeSolo>, "renderInput" | "onChange">;
