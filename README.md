# @uxf/cms
[![npm](https://img.shields.io/npm/v/@uxf/cms)](https://www.npmjs.com/package/@uxf/cms)
[![size](https://img.shields.io/bundlephobia/min/@uxf/cms)](https://www.npmjs.com/package/@uxf/cms)
[![quality](https://img.shields.io/npms-io/quality-score/@uxf/cms)](https://www.npmjs.com/package/@uxf/cms)
[![license](https://img.shields.io/npm/l/@uxf/cms)](https://www.npmjs.com/package/@uxf/cms)

## Použití balíčku

Konfigurace balíčku se prvádí v souboru `_app.tsx`.

```tsx
// _app.tsx

import { config, container } from "@uxf/cms/config";

cmsConfig.set("api-url", "== api url ==");

container
    .register("router", Router<any>)
    .register("service.error", ErrorService)
    .register("service.notification", FlashMessagesService)
    .register("useLoggedUser", UseLoggedUser);
```

## Api
- cookie je předávána automaticky z kontextu

```tsx
import {...} from  "@uxf/cms/api";
```

- `getLoggedUser`
- `getEntityMetaSchemas`
- `getFormSchema`
- `getFormValues`
- `login`
- `autocompleteRows`
- `saveFormValues`
- `userConfigSave`
- `userConfigGetAll`
- `userConfigGet`

## UI

### Autocomplete

// TODO

### Button
```tsx
import { Button } from "@uxf/cms/ui/Button";

<Button loading={true}>title</Button>
```

### CopyToClipboard
```tsx
import { CopyToClipboard } from "@uxf/cms/ui/CopyToClipboard";

<CopyToClipboard value="https://" label="Zkopírovat url adresu" disabled />
```

### CopyToClipboardButton
```tsx
import { CopyToClipboardButton } from "@uxf/cms/ui/CopyToClipboardButton";

<CopyToClipboardButton textToCopy="Text ke zkopírování"/>
```

### FileInput
nativní fileInput bez uploadu (přijímá a vrací object typu File)

```tsx
import { FileInput } from "@uxf/cms/ui/FileInput";

<FileInput id="file-input-id" name="fileInput" label="..." placeholder="..." value={value} onChange={setValue} />
```

### Label
```tsx
import { Label } from "@uxf/cms/ui/Label";

<Label color="success">SUCCESS</Label>
```

### Paper
```tsx
import { Paper } from "@uxf/cms/ui/Paper";

<Paper mr={1} ml={2} mt={3} mb={4} ph={1} pv={2}>content</Paper>
```

### Select

// TODO

## Pages

### ForgottenPasswordPage

```tsx
import { ForgottenPasswordPage } from "@uxf/cms/pages/ForgottenPasswordPage";

const Page = ForgottenPasswordPage({
    title: "Zapomenuté heslo",
    onSubmit: (username) => {
        /* implement me */
        return Promise.resolve();
    },
});
```

### FormPage

```tsx
import { FormPage } from "@uxf/cms/pages/FormPage";

export default FormPage({
    entityAlias: "user",
});
```

### LoginPage

```tsx
import { LoginPage } from "@uxf/cms/pages/LoginPage";

export default LoginPage({
    pageTitle: "Přihlášení | UXFans",
    onLoginDone: (response, redirectUrl) => {
        console.log("Login done!");
        return Promise.resolve();
    },
});
```

## FlashMessages

```tsx
// pages/_app.tsx

import { FlashMessagesContainerInstance } from "@uxf/cms/lib/FlashMessages";

function App(props: any) {
    const { Component, pageProps } = props;

    return (
        <>
            <Component {...pageProps} />
            <FlashMessagesContainerInstance />
        </>
    );
}

export default App;
```

```tsx
// pages/index.tsx

import { FlashMessagesService } from "@uxf/cms/services/FlashMessagesService";

function IndexPage(props) {
    return <Button onClick={() => FlashMessagesService.success("Hotovo.")}>Klikni</Button>;
}

export default IndexPage;
```

## Jak vyvíjet?
```bash
git clone git@gitlab.com:uxf-npm/cms.git

cd ./cms

npm install
```

## Spuštění examplu:

```bash
npm run dev
```

## Vydání balíčku:

Balíček se vydá po mergi do `master` branche.

K publikování nových verzí do npm je opužit balíček [semantic-release](https://github.com/semantic-release/semantic-release) .

### Jak má vypadat commit message?

https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines

| Commit message                                                                                                                                                                                   | Release type               |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------|
| `fix(pencil): stop graphite breaking when too much pressure applied`                                                                                                                             | Patch Release              |
| `feat(pencil): add 'graphiteWidth' option`                                                                                                                                                       | ~~Minor~~ Feature Release  |
| `perf(pencil): remove graphiteWidth option`<br><br>`BREAKING CHANGE: The graphiteWidth option has been removed.`<br>`The default graphite width of 10mm is always used for performance reasons.` | ~~Major~~ Breaking Release |
