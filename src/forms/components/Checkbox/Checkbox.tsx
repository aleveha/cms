import React from "react";
import MuiCheckbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import { useField } from "react-final-form";
import { CheckboxProps } from "./types";
import FormControl from "@mui/material/FormControl";
import FormHelperText from "@mui/material/FormHelperText";
import Box from "@mui/material/Box";

export const Checkbox: React.FC<CheckboxProps> = (props) => {
    const { name, id, label, disabled, required, helperText } = props;
    const { input } = useField(name, { type: "checkbox" });

    return (
        <FormControl>
            <FormControlLabel
                label={
                    <>
                        {label}
                        {required ? " *" : ""}
                    </>
                }
                disabled={disabled}
                htmlFor={id}
                control={
                    <MuiCheckbox
                        onChange={input.onChange}
                        checked={input.checked}
                        required={required}
                        disabled={disabled}
                        id={id}
                        onBlur={input.onBlur}
                        onFocus={input.onFocus}
                    />
                }
            />
            {helperText && (
                <Box mt={-1}>
                    <FormHelperText>{helperText}</FormHelperText>
                </Box>
            )}
        </FormControl>
    );
};
