import MuiButton from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import React, { FC } from "react";
import { ButtonProps } from "./types";

export const Button: FC<ButtonProps> = (props) => {
    const { loading, children, mt, mb, ml, mr, sx, ...rest } = props;
    return (
        <MuiButton sx={{ ...sx, ml, mr, mb, mt }} {...rest}>
            {children}
            {!!loading && (
                <CircularProgress color="inherit" size={20} sx={{ marginLeft: (theme) => theme.spacing(1) }} />
            )}
        </MuiButton>
    );
};
