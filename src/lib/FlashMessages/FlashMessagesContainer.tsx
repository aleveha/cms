import { Root } from "./styles";
import React from "react";
import { FlashMessage } from "./FlashMessage";
import { FlashMessageConfig, FlashMessagesContainerState } from "./types";

const NOTIFICATION_HEIGHT = 48;
const NOTIFICATION_OFFSET = 12;
const VIEW_OFFSET = 20;

function getFlashMessageOffset(index: number) {
    return VIEW_OFFSET + index * (NOTIFICATION_HEIGHT + NOTIFICATION_OFFSET);
}

export class FlashMessagesContainer extends React.Component<{}, FlashMessagesContainerState> {
    constructor(props: Readonly<{}>) {
        super(props);

        this.state = {
            items: [],
        };
    }

    public open = (config: FlashMessageConfig) => {
        this.setState((state) => ({
            items: [
                ...state.items,
                {
                    config: {
                        ...config,
                        id: config.id ?? Math.round(Math.random() * 1000),
                    },
                    closing: false,
                },
            ],
        }));
    };

    private remove = (config: FlashMessageConfig) => {
        this.setState((state) => ({
            items: state.items.filter((i) => i.config.id !== config.id),
        }));
    };

    public close = (config: FlashMessageConfig) => {
        this.setState(
            (state) => ({
                items: state.items.map((i) => (i.config.id === config.id ? { ...i, closing: true } : i)),
            }),
            () => setTimeout(() => this.remove(config), 200),
        );
    };

    public render() {
        const { items } = this.state;
        return (
            <Root>
                {items.map((item, index) => (
                    <FlashMessage
                        key={item.config.id}
                        onClose={this.close}
                        offset={getFlashMessageOffset(index)}
                        closing={item.closing}
                        config={item.config}
                    />
                ))}
            </Root>
        );
    }
}
