import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import React from "react";
import { SelectProps } from "./types";

export const Select = <
    T,
    Multiple extends boolean | undefined = undefined,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
>(
    props: SelectProps<T, Multiple, DisableClearable, FreeSolo>,
) => {
    const { label, TextFieldProps = {}, ...autocompleteProps } = props;
    return (
        <Autocomplete
            isOptionEqualToValue={(o: any, v: any) => (o.id ?? o.value) === (v.id ?? v.value)}
            getOptionLabel={(o: any) => o.label}
            {...autocompleteProps}
            renderInput={(params) => (
                <TextField {...params} label={label} variant="outlined" size="small" {...TextFieldProps} />
            )}
        />
    );
};
