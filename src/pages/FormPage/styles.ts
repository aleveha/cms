import MuiFab from "@mui/material/Fab";
import { styled } from "@mui/material/styles";

export const Fab: any = styled(MuiFab)(({ theme }) => ({
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
        bottom: theme.spacing(3),
        right: theme.spacing(3),
    },
}));
