import { Reducer } from "react";

type Action =
    | { type: "SEARCH"; term: string }
    | { type: "LOADING_DONE"; options: any[]; loadingId: number }
    | { type: "LOADING_ERROR"; error: any; loadingId: number };

type State = {
    term: string;
    options: any[];
    error: any;
    loading: boolean;
    loadingId: number;
};

export const reducer: Reducer<State, Action> = (prevState, action) => {
    switch (action.type) {
        case "SEARCH":
            return {
                ...prevState,
                term: action.term,
                loading: true,
                loadingId: prevState.loadingId + 1,
            };
        case "LOADING_DONE":
            return action.loadingId === prevState.loadingId
                ? {
                      ...prevState,
                      error: null,
                      options: action.options,
                      loading: false,
                  }
                : prevState;
        case "LOADING_ERROR":
            return action.loadingId === prevState.loadingId
                ? {
                      ...prevState,
                      options: [],
                      loading: false,
                      error: action.error,
                  }
                : prevState;
    }
};
