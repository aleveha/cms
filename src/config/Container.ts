import { ReactNode } from "react";
import { Router } from "@uxf/router";

export interface NotificationServiceInterface {
    info: (message: ReactNode) => void;
    success: (message: ReactNode) => void;
    warning: (message: ReactNode) => void;
    error: (message: ReactNode) => void;
}

export interface ErrorServiceInterface {
    handleError: (e: any) => void;
    logError: (e: any) => void;
}

export interface LoggedUser {
    roles: string[];
}

export type UseLoggedUser = () => LoggedUser;

type ContainerStore = {
    router: Router<any>;
    useLoggedUser: UseLoggedUser;
    "service.notification": NotificationServiceInterface;
    "service.error": ErrorServiceInterface;
};

export class Container {
    private store: Partial<ContainerStore> = {};

    public register<T extends keyof ContainerStore>(key: T, value: ContainerStore[T]): Container {
        this.store[key] = value;
        return this;
    }

    public get<T extends keyof ContainerStore>(key: T): ContainerStore[T] {
        const value = this.store[key];

        if (!value) {
            throw new Error(`Container has no service '${key}'.`);
        }

        return value as any;
    }
}
