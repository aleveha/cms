import { menu } from "../../config";
import { Layout } from "../../src/lib/Layout";
import { Paper } from "../../src/ui/Paper";
import React, { useCallback } from "react";
import { UserInvitationForm, UserInvitationFormProps } from "../../src/forms/UserInvitationForm";

const Page = () => {
    const onSubmit = useCallback<UserInvitationFormProps<any>["onSubmit"]>((values) => {
        console.log(values);
        return Promise.resolve(values);
    }, []);

    return (
        <Layout title="Pozvat nového uživatele" menuConfiguration={menu}>
            <Paper>
                <UserInvitationForm
                    onSubmit={onSubmit}
                    onLoadRoles={() => Promise.resolve([{ id: "ROLE_ROOT", label: "ROLE_ROOT" }])}
                />
            </Paper>
        </Layout>
    );
};

export default Page;
