import MuiDatePicker from "@mui/lab/DatePicker";
import TextField from "@mui/material/TextField";
import React from "react";
import { DatePickerProps } from "./types";

export const DatePicker: React.FC<DatePickerProps> = (props) => {
    const { disabled, label, required, error, onBlur, onFocus, variant = "outlined", ...inputProps } = props;

    return (
        <MuiDatePicker
            clearable
            showTodayButton
            renderInput={(params) => (
                <TextField
                    {...params}
                    helperText={error}
                    error={!!error}
                    variant={variant}
                    size="small"
                    onBlur={onBlur}
                    onFocus={onFocus}
                    fullWidth
                />
            )}
            label={
                <>
                    {label}
                    {required ? " *" : ""}
                </>
            }
            disabled={disabled}
            onClose={onBlur}
            onOpen={onFocus}
            {...inputProps}
        />
    );
};
