import { createRef, ReactNode } from "react";
import { FlashMessagesContainer } from "../lib/FlashMessages/FlashMessagesContainer";

export const flashMessagesRef = createRef<FlashMessagesContainer>();

function getRefInstance(): FlashMessagesContainer {
    if (!flashMessagesRef.current) {
        throw new Error("Flash messages container reference is not initialized.");
    }

    return flashMessagesRef.current;
}

function info(body: ReactNode): void {
    getRefInstance().open({
        body,
        variant: "info",
    });
}

function success(body: ReactNode): void {
    getRefInstance().open({
        body,
        variant: "success",
    });
}

function warning(body: ReactNode): void {
    getRefInstance().open({
        body,
        variant: "warning",
    });
}

function error(body: ReactNode): void {
    getRefInstance().open({
        body,
        variant: "error",
    });
}

export const FlashMessagesService = {
    info,
    success,
    warning,
    error,
};
