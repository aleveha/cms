import Typography from "@mui/material/Typography";
import { getRouteByUrl, Router } from "../../routes";
import { LoginPage } from "../../src/pages/LoginPage";

const Page = LoginPage({
    pageTitle: "Přihlášení | UXFans",
    title: "Přihlášení",
    onLoginDone: (_, redirectUrl) => {
        const [route, params] = getRouteByUrl(redirectUrl || "/");
        Router.push(route, params);
        return Promise.resolve();
    },
    onForgottenPassword: () => console.log("Forgotten password."),
    ui: {
        hideTitle: true,
        Logo: <Typography>Logo</Typography>,
    },
});

export default Page;
