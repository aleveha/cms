import React, { FC, useContext, useEffect, useState } from "react";
import { CmsContextType } from "./types";

const CmsContext = React.createContext<CmsContextType>({
    sidebar: {
        isOpen: false,
        open: () => null,
        close: () => null,
    },
});

export const useCmsContext = <T extends keyof CmsContextType>(key: T): CmsContextType[T] => {
    return useContext(CmsContext)[key];
};

export const CmsProvider: FC = (props) => {
    const { children } = props;

    const [isOpen, setIsOpen] = useState(true);
    const [isMobile, setIsMobile] = useState(false);

    useEffect(() => {
        if (window.innerWidth < 1280) {
            setIsMobile(true);
        }
        const onResize = () => setIsMobile(window.innerWidth < 1280);
        window.addEventListener("resize", onResize);
        return () => {
            window.removeEventListener("resize", onResize);
        };
    }, []);

    useEffect(() => {
        if (isMobile) {
            setIsOpen(false);
        } else {
            setIsOpen(window.localStorage.getItem("cms.sidebar.isOpen") === "1");
        }
    }, [isMobile]);

    useEffect(() => {
        if (!isMobile) {
            window.localStorage.setItem("cms.sidebar.isOpen", isOpen ? "1" : "0");
        }
    }, [isOpen, isMobile]);

    const value: CmsContextType = {
        sidebar: {
            isOpen,
            open: () => setIsOpen(true),
            close: () => setIsOpen(false),
        },
    };

    return <CmsContext.Provider value={value}>{children}</CmsContext.Provider>;
};
