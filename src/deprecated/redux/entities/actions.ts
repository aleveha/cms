import { EntityMetaSchema } from "../../../api";
import { EntityFormSchema } from "../../types/Form";
import { ActionsUnion, createAction } from "../reduxHelper";
import { Schema } from "@uxf/data-grid";

export interface IState {
    meta: Record<string, EntityMetaSchema>;
    form: Record<string, EntityFormSchema>;
    grid: Record<string, Schema<any>>;
}

export const onSetEntityMetaSchemas = (meta: Record<string, EntityMetaSchema>) =>
    createAction("entities/SET_META_SCHEMAS", meta);

export const onSetFormSchema = (entityAlias: string, form: EntityFormSchema) =>
    createAction("entities/SET_FORM_SCHEMA", { entityAlias, form });

export const onSetGridSchema = (entityAlias: string, grid: Schema<any>) =>
    createAction("entities/SET_GRID_SCHEMA", { entityAlias, grid });

export const Actions = {
    onSetEntityMetaSchemas,
    onSetFormSchema,
    onSetGridSchema,
};

export type ActionTypes = ActionsUnion<typeof Actions>;
