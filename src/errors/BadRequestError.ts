import ExtendableError from "es6-error";

export class BadRequestError extends ExtendableError {
    public data: any;

    constructor(data: { message?: string; shouldTranslate?: boolean }, message: string = "Bad Request") {
        super(data.message ?? message);
        this.data = data;
    }
}
