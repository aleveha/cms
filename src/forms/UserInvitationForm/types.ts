import { FormProps } from "react-final-form";
import { AutocompleteProps } from "../components/Autocomplete";

export interface UserInvitationFormData<T> {
    name: string;
    surname: string;
    email: string;
    roles: T[];
}

export interface UserInvitationFormProps<T> extends FormProps<UserInvitationFormData<T>> {
    onLoadRoles: AutocompleteProps<T>["loadOptions"];
}
