import { NextPageContext } from "next";
import { Store } from "redux";
import { IReduxState } from "../redux/types";

export interface IAppContext extends NextPageContext {
    reduxStore: Store<IReduxState>;
}
