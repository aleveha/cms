import React, { useCallback, useRef, useState } from "react";
import TextField from "@mui/material/TextField";
import Tooltip from "@mui/material/Tooltip";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import FileCopy from "@mui/icons-material/FileCopy";
import { CopyToClipboardProps } from "./types";

export const CopyToClipboard: React.FC<CopyToClipboardProps> = (props) => {
    const { label, value, disabled } = props;

    const inputRef = useRef<any>();

    const [tooltipVisible, setTooltipVisible] = useState(false);

    const onCopy = useCallback(() => {
        setTooltipVisible(true);

        inputRef.current.select();
        inputRef.current.setSelectionRange(0, 99999); // for mobile devices
        document.execCommand("copy");

        setTimeout(() => {
            setTooltipVisible(false);
        }, 3000);
    }, []);

    return (
        <TextField
            inputRef={inputRef}
            variant="outlined"
            size="small"
            fullWidth
            value={value}
            label={label}
            disabled={disabled}
            InputLabelProps={{
                shrink: true,
            }}
            InputProps={{
                readOnly: true,
                endAdornment: (
                    <Tooltip open={tooltipVisible} title="Text zkopírován" placement="left">
                        <InputAdornment position="end">
                            <IconButton
                                disabled={disabled}
                                onClick={onCopy}
                                size="small"
                                aria-label="Kopírovat text"
                                edge="end"
                            >
                                <FileCopy />
                            </IconButton>
                        </InputAdornment>
                    </Tooltip>
                ),
            }}
        />
    );
};
