export interface FieldSchema {
    name: string;
    type: string;
    label: string;
    required: boolean;
    readOnly: boolean;
    editable: boolean;
    fields: FieldSchema[] | null;
    autocompleteName?: string; // filled manually for one to one fieldset autocomplete
}

export interface EntityFormSchema {
    fields: FieldSchema[];
}

export enum FormTypes {
    TEXT = "text",
    BOOLEAN = "boolean",
    DATE = "date",
    DATETIME = "datetime",
    MANY_TO_MANY = "manyToMany",
    MANY_TO_ONE = "manyToOne",
    ONE_TO_MANY = "oneToMany",
    ONE_TO_ONE = "oneToOne",
    ONE_TO_ONE_FIELDSET = "oneToOneFieldset",
    EMBEDDED = "embedded",
}
