import Grid from "@mui/material/Grid";
import SaveIcon from "@mui/icons-material/Save";
import { FORM_ERROR } from "final-form";
import arrayMutators from "final-form-arrays";
import { NextPage } from "next";
import Router from "next/router";
import React, { useCallback, useEffect, useState } from "react";
import { Form, FormProps } from "react-final-form";
import {
    EntityMetaSchema,
    FormSchemaResponse,
    getEntityMetaSchemas,
    getFormSchema,
    getFormValues,
    saveFormValues,
} from "../../api";
import { container } from "../../config";
import { withAuthenticate } from "../../deprecated/hoc/withAuthenticate";
import { ValidationError } from "../../errors/ValidationError";
import { Paper } from "../../ui/Paper";
import { BaseField } from "./BaseField";
import { Fab } from "./styles";
import { FieldProps, FormPageConfig } from "./types";

export interface InitialProps<T extends { id?: number | null }> {
    formSchema: FormSchemaResponse;
    formValues: T;
    entityAlias: string;
    entityId: number | null;
    entityMetaSchema: EntityMetaSchema;
}

const resolve = <T extends string | number>(
    value: T | undefined | null | ((value: T) => T | null),
    ctx: any,
): T | null => (typeof value === "function" ? value(ctx) : value ?? null);

export const FormPage = <T extends { id?: number | null }>(config: FormPageConfig<T>) => {
    const entityIdResolver = config.entityId ?? (({ query }) => (query.id ? Number.parseInt(query.id) : null));
    const entityAliasResolver =
        config.entityAlias ?? (({ query }) => (query.entityAlias ? `${query.entityAlias}` : null));

    const onSaveDone = config.onSaveDone ?? Router.back;

    const Component: NextPage<InitialProps<T>> = (props) => {
        const { formSchema, entityAlias, entityId, entityMetaSchema } = props;
        const [initialValues, setInitialValues] = useState<T>(props.formValues);

        useEffect(() => {
            setInitialValues(props.formValues);
        }, [props.formValues]);

        const onSubmit = useCallback<FormProps<T>["onSubmit"]>(
            async (values) => {
                try {
                    const newInitialValues = config.onSave
                        ? await config.onSave(values, entityAlias)
                        : await saveFormValues<T>(null, { entityAlias, id: entityId }, values).then((r) => r.data);

                    setInitialValues(newInitialValues);

                    onSaveDone(newInitialValues, entityAlias);

                    return;
                } catch (e) {
                    if (e instanceof ValidationError) {
                        console.error(e);
                        return e.fields;
                    }
                    container.get("service.error").handleError(e);
                    return { [FORM_ERROR]: "Error" }; // TODO
                }
            },
            [entityAlias, entityId, setInitialValues],
        );

        const FieldComponent: React.FC<FieldProps> = config.ui?.Field ?? BaseField;
        const Layout = config.ui?.Layout ?? <div />;

        const Content = (
            <Paper>
                <Form initialValues={initialValues} onSubmit={onSubmit} mutators={{ ...arrayMutators }}>
                    {({ handleSubmit, submitting, pristine }) => (
                        <form onSubmit={handleSubmit}>
                            <Grid container spacing={3}>
                                {formSchema.fields.map((field, i) => (
                                    <Grid item key={i} xs={12}>
                                        <FieldComponent
                                            fieldSchema={field}
                                            entityAlias={entityAlias}
                                            isEditing={!!entityId}
                                        />
                                    </Grid>
                                ))}
                            </Grid>
                            <Fab title="Uložit" type="submit" disabled={submitting || pristine}>
                                <SaveIcon />
                            </Fab>
                        </form>
                    )}
                </Form>
            </Paper>
        );

        return React.cloneElement(typeof Layout === "function" ? Layout(entityMetaSchema) : Layout, {
            children: Content,
        });
    };

    Component.getInitialProps = async (ctx) => {
        const entityId = resolve(entityIdResolver, ctx);
        const entityAlias = resolve(entityAliasResolver, ctx) ?? "";

        const metaSchemas = await getEntityMetaSchemas(ctx).then((r) => r.data);

        if (!metaSchemas[entityAlias]) {
            throw new Error(`Entity meta schema for alias ${entityAlias} does not exist.`);
        }

        const formSchema = await getFormSchema(ctx, { entityAlias }).then((r) => r.data);

        return {
            entityId,
            entityAlias,
            entityMetaSchema: metaSchemas[entityAlias],
            formSchema: config.configureSchema ? config.configureSchema(formSchema) : formSchema,
            formValues: entityId ? await getFormValues(ctx, { entityAlias, id: entityId }).then((r) => r.data) : {},
        };
    };

    return withAuthenticate({ allowedRoles: config.allowedRoles || ["ROLE_ROOT"] })(Component);
};
