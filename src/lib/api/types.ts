import { AxiosPromise } from "axios";
import { ResponseValues } from "axios-hooks";

type RequestConfigBody<T extends {} | null> = T extends null
    ? {}
    : {
          body: T;
      };

type RequestConfigQuery<T extends {} | null> = T extends null
    ? {}
    : {
          query: T;
      };

type RequestConfigPath<T extends {} | null> = T extends null
    ? {}
    : {
          path: T;
      };

export type RequestConfig<
    RequestBody extends {} | null,
    RequestQuery extends {} | null,
    RequestPath extends {} | null,
> = {} & RequestConfigBody<RequestBody> & RequestConfigQuery<RequestQuery> & RequestConfigPath<RequestPath>;

export type Execute<
    Response extends {},
    RequestBody extends {} | null,
    RequestQuery extends {} | null,
    RequestPath extends {} | null,
> = (config: RequestConfig<RequestBody, RequestQuery, RequestPath>) => AxiosPromise<Response>;

export type UseAxiosRequest<
    Response extends {},
    RequestBody extends {},
    RequestQuery extends {},
    RequestPath extends {},
> = (
    config: RequestConfig<RequestBody, RequestQuery, RequestPath>,
) => [ResponseValues<Response, any>, Execute<Response, RequestBody, RequestQuery, RequestPath>];
