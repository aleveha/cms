export default class CmsAuthorizator {
    public static isGranted(requiredRoles: string[], userRoles: string[]): boolean {
        return requiredRoles.some((role: string) => userRoles.includes(role));
    }
}
