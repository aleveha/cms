import { TextFieldProps } from "@mui/material";
import { FieldValidator } from "final-form";
import { ReactNode } from "react";

export interface TextInputProps extends Omit<TextFieldProps, "name" | "onChange" | "value"> {
    name: string;
    validate?: FieldValidator<string>[];
    requiredErrorMessage?: ReactNode;
    helperText?: ReactNode;
}
