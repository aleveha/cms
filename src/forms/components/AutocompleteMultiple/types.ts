import { AutocompleteMultipleProps as UIAutocompleteMultipleProps } from "../../../ui/AutocompleteMultiple";
import { ReactNode } from "react";

interface ExtraAutocompleteProps {
    name: string;
    required?: boolean;
    requiredErrorMessage?: ReactNode;
}

export type AutocompleteMultipleProps<
    T,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined
> = Omit<UIAutocompleteMultipleProps<T, DisableClearable, FreeSolo>, "onChange" | "name" | "value" | "multiple"> &
    ExtraAutocompleteProps;
