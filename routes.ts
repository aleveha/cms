import { Router as UxfRouter } from "@uxf/router";

export interface RouteList {
    index: null;
    form: null;
    "loader": null;
    "forms/user-invitation": null;
    "pages/form-page": null;
    "pages/forgotten-password-page": null;
    "pages/login-page": null;
    "pages/renew-password-page": null;
    "pages/grid-page": null;
}

const router = new UxfRouter<RouteList>({
    index: "/",
    form: "/form/komponenty",
    loader: "/loader",
    "forms/user-invitation": "/form/user-invitation",
    "pages/form-page": "/pages/form/:id?",
    "pages/forgotten-password-page": "/pages/forgotten-password",
    "pages/login-page": "/pages/login",
    "pages/renew-password-page": "/pages/renew-password",
    "pages/grid-page": "/pages/grid-page",
});

export const Link = router.getLink();
export const useRouter = router.getUseRouter();
export const Router = router.getSingletonRouter();
export const getRequestHandler = router.getRequestHandler;
export type RouteName = keyof RouteList;
export const uxfRouter = router;
export const getRouteByUrl = router.getRouteByUrl;
