import MuiAppBar from "@mui/material/AppBar";
import MuiToolbar from "@mui/material/Toolbar";
import React, { FC } from "react";
import { config } from "../../config";
import { LogoContainer } from "./styles";
import { AppBarProps } from "./types";

export const AppBar: FC<AppBarProps> = (props) => {
    const { Logo, ToolbarRight } = props;
    return (
        <MuiAppBar
            position="fixed"
            sx={{
                zIndex: (theme) => theme.zIndex.drawer + 1,
                width: `100%`,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
            }}
        >
            <LogoContainer>{Logo ?? config.get("app-name")}</LogoContainer>
            <MuiToolbar
                sx={{
                    flex: 1,
                    justifyContent: "flex-end",
                }}
                variant="dense"
            >
                {ToolbarRight}
            </MuiToolbar>
        </MuiAppBar>
    );
};
