import { ReactNode } from "react";
import { LoginResponse } from "../../api";

export interface InitialProps {
    redirectUrl?: string;
}

export interface LoginPageConfig {
    pageTitle?: string;
    title: ReactNode;
    onForgottenPassword?: () => void;
    onLoginDone: (loginResponse: LoginResponse, redirectUrl?: string) => Promise<void>;
    ui?: {
        hideTitle?: boolean;
        Logo?: ReactNode;
    };
}

export interface LoginPageQueryParams {
    redirect?: string;
}
