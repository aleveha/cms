import { PaperProps as MuiPaperProps } from "@mui/material/Paper";

export interface PaperProps extends MuiPaperProps {
    mt?: number;
    mb?: number;
    ml?: number;
    mr?: number;
    ph?: number;
    pv?: number;
}
