import { ReactNode } from "react";
import { SelectProps as UISelectProps } from "../../../ui/Select";

export type ExtraSelectProps<
    T,
    Multiple extends boolean | undefined,
    DisableClearable extends boolean | undefined,
    FreeSolo extends boolean | undefined,
> = {
    name: string;
    required?: boolean;
    requiredErrorMessage?: ReactNode;
    onChange?: UISelectProps<T, Multiple, DisableClearable, FreeSolo>["onChange"];
};

export type SelectProps<
    T,
    Multiple extends boolean | undefined,
    DisableClearable extends boolean | undefined,
    FreeSolo extends boolean | undefined,
> = Omit<UISelectProps<T, Multiple, DisableClearable, FreeSolo>, "onChange" | "value" | "name"> &
    ExtraSelectProps<T, Multiple, DisableClearable, FreeSolo>;
