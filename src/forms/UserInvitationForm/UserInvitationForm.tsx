import Grid from "@mui/material/Grid";
import React, { ReactElement } from "react";
import { Form } from "react-final-form";
import { Button } from "../../ui/Button";
import { AutocompleteMultiple } from "../components/AutocompleteMultiple";
import { TextInput } from "../components/TextInput";
import { UserInvitationFormProps } from "./types";

export const UserInvitationForm = <T extends any>(props: UserInvitationFormProps<T>): ReactElement => {
    const { onLoadRoles, ...formProps } = props;

    return (
        <Form {...formProps}>
            {({ handleSubmit, submitting }) => (
                <form onSubmit={handleSubmit}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <TextInput label="Jméno" name="name" required />
                        </Grid>
                        <Grid item xs={12}>
                            <TextInput label="Příjmení" name="surname" required />
                        </Grid>
                        <Grid item xs={12}>
                            <TextInput label="E-mail" name="email" required />
                        </Grid>
                        <Grid item xs={12}>
                            <AutocompleteMultiple name="roles" label="Role" required loadOptions={onLoadRoles} />
                        </Grid>
                        <Grid item xs={12}>
                            <Button
                                color="primary"
                                disabled={submitting}
                                loading={submitting}
                                type="submit"
                                variant="contained"
                            >
                                Vytvořit uživatele a poslat mu pozvánku
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            )}
        </Form>
    );
};
