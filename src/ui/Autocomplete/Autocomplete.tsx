import CircularProgress from "@mui/material/CircularProgress";
import TextField from "@mui/material/TextField";
import MuiAutocomplete from "@mui/material/Autocomplete";
import React, { useEffect, useReducer } from "react";
import { reducer } from "./reducer";
import { AutocompleteProps } from "./types";

export const Autocomplete = <
    T,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
>(
    props: AutocompleteProps<T, DisableClearable, FreeSolo>,
): JSX.Element => {
    const { label, placeholder, loadOptions, TextFieldProps = {}, ...autocompleteProps } = props;

    const [state, dispatch] = useReducer(reducer, {
        term: "",
        error: null,
        options: [],
        loading: false,
        loadingId: 1,
    });

    useEffect(() => {
        const loadingId = state.loadingId;
        loadOptions(state.term)
            .then((options) => dispatch({ type: "LOADING_DONE", loadingId, options }))
            .catch((error) => dispatch({ type: "LOADING_ERROR", loadingId, error }));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [state.loadingId]);

    return (
        <MuiAutocomplete<T, false, DisableClearable, FreeSolo>
            options={state.options}
            loading={state.loading}
            onInputChange={(_, term) => dispatch({ type: "SEARCH", term })}
            inputValue={state.term}
            size="small"
            filterOptions={(o) => o}
            noOptionsText={state.error ? "Nepodařilo se načíst data" : "Žádné záznamy nenalezeny"}
            loadingText="Načítání ..."
            fullWidth
            isOptionEqualToValue={(o: any, v: any) => (o.id ?? o.value) === (v.id ?? v.value)}
            getOptionLabel={(o: any) => o.label}
            renderInput={(params) => (
                <TextField
                    placeholder={placeholder}
                    label={label}
                    variant="outlined"
                    {...params}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <>
                                {state.loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </>
                        ),
                    }}
                    {...TextFieldProps}
                />
            )}
            {...autocompleteProps}
        />
    );
};
