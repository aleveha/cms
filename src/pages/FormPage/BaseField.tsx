import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import React, { Fragment } from "react";
import { autocompleteRows } from "../../api";
import { Autocomplete } from "../../forms/components/Autocomplete";
import { AutocompleteMultiple } from "../../forms/components/AutocompleteMultiple";
import { Checkbox } from "../../forms/components/Checkbox";
import { DatePicker } from "../../forms/components/DatePicker";
import { DateTimePicker } from "../../forms/components/DateTimePicker";
import { TextInput } from "../../forms/components/TextInput";
import { FieldProps } from "./types";
import { Field } from "react-final-form";
import { FieldArray } from "react-final-form-arrays";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";
import { Button } from "../../ui/Button";

export const BaseField: React.FC<FieldProps> = (props) => {
    const { fieldSchema, entityAlias, isEditing, prefix = "" } = props;

    const { editable, label, name, readOnly, type } = fieldSchema;
    const fieldName = `${prefix}${name}`;

    const disabled = readOnly || (isEditing && !editable);
    const required = !disabled && fieldSchema.required;

    switch (type) {
        case "boolean":
            return <Checkbox name={fieldName} label={label} required={required} disabled={disabled} />;
        case "date":
            return <DatePicker name={fieldName} label={label} required={required} disabled={disabled} />;
        case "datetime":
            return <DateTimePicker name={fieldName} label={label} required={required} disabled={disabled} />;
        case "manyToMany":
            return (
                <AutocompleteMultiple
                    name={fieldName}
                    label={label}
                    required={required}
                    disabled={disabled}
                    loadOptions={(term: string) =>
                        autocompleteRows(null, { entityAlias, fieldName: fieldSchema.name }, { term }).then(
                            (r) => r.data,
                        )
                    }
                />
            );
        case "manyToOne":
            return (
                <Autocomplete
                    name={fieldName}
                    label={label}
                    required={required}
                    disabled={disabled}
                    loadOptions={(term) =>
                        autocompleteRows(null, { entityAlias, fieldName: fieldSchema.name }, { term }).then(
                            (r) => r.data,
                        )
                    }
                />
            );
        case "oneToMany":
            if (fieldSchema.fields === null) {
                return null;
            }

            return (
                <>
                    <Typography color="textSecondary" variant="caption">
                        {fieldSchema.label}
                    </Typography>
                    <FieldArray name={fieldName}>
                        {({ fields }) => (
                            <>
                                <Grid container spacing={1}>
                                    {fields.map((name, index) => (
                                        <Fragment key={`${name}-${index}`}>
                                            {fieldSchema.fields?.map((schema, i) => {
                                                if (schema.name === "id") {
                                                    return (
                                                        <Field
                                                            key={`${schema.name}-${i}`}
                                                            name="id"
                                                            component="input"
                                                            hidden
                                                        />
                                                    );
                                                }

                                                return (
                                                    <Grid item xs={5} key={`${schema.name}-${i}`}>
                                                        <BaseField
                                                            fieldSchema={schema}
                                                            entityAlias={entityAlias}
                                                            isEditing={isEditing}
                                                            prefix={`${name}.`}
                                                        />
                                                    </Grid>
                                                );
                                            })}
                                            <Grid item xs={2}>
                                                <Button
                                                    mt={2}
                                                    startIcon={<DeleteIcon />}
                                                    variant="contained"
                                                    color="secondary"
                                                    size="small"
                                                    onClick={() => fields.remove(index)}
                                                >
                                                    Remove
                                                </Button>
                                            </Grid>
                                        </Fragment>
                                    ))}
                                </Grid>
                                <Button
                                    variant="contained"
                                    size="small"
                                    color="primary"
                                    startIcon={<AddIcon />}
                                    onClick={() => fields.push({ id: null })}
                                    mt={2}
                                >
                                    Add
                                </Button>
                            </>
                        )}
                    </FieldArray>
                </>
            );
        case "embedded":
            if (fieldSchema.fields === null) {
                return null;
            }

            return (
                <>
                    {fieldSchema.fields.map((schema, i) => (
                        <Box sx={{ mt: i === 0 ? undefined : 2 }} key={i}>
                            <BaseField
                                {...props}
                                fieldSchema={{ ...schema, label: `${fieldSchema.label} ${schema.label}` }}
                                prefix={`${fieldSchema.name}.`}
                            />
                        </Box>
                    ))}
                </>
            );
        case "text":
            return <TextInput name={fieldName} label={label} required={required} disabled={disabled} multiline />;
        default:
            return <TextInput name={fieldName} label={label} required={required} disabled={disabled} />;
    }
};
