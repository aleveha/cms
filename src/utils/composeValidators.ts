export const composeValidators = (...validators: any[]) => (...params: any[]): string | undefined =>
    validators.reduce((error, validator) => (validator ? error || validator(...params) : error), undefined);
