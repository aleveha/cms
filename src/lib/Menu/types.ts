import { UxfRouter } from "@uxf/router";
import { MenuItem } from "./model";

export type RouterResolver = (router: UxfRouter<any>) => boolean;
export type Resolver = (item: MenuItem) => RouterResolver;

export type MenuConfiguration = MenuItem[];
