import Drawer from "@mui/material/Drawer";
import MuiToolbar from "@mui/material/Toolbar";
import React, { FC } from "react";
import { Menu } from "../Menu";
import { SidebarProps } from "./types";

const drawerWidth = 240;

export const Sidebar: FC<SidebarProps> = (props) => {
    const { Menu: MenuComponent, menuConfiguration, UserMenu } = props;
    // const { isOpen } = useCmsContext("sidebar");

    return (
        <Drawer
            sx={{
                flexShrink: 0,
                width: drawerWidth,
                "& .MuiDrawer-paper": {
                    boxSizing: "border-box",
                    width: drawerWidth,
                },
            }}
            variant="permanent"
            anchor="left"
        >
            <MuiToolbar variant="dense" />
            {MenuComponent ?? (menuConfiguration ? <Menu menu={menuConfiguration} /> : null)}
            {UserMenu}
        </Drawer>
    );
};
