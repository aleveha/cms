import { useCallback, useState } from "react";
import { userConfigSave } from "../../api";

export const useHiddenColumns = (entityAlias: string, initialHiddenColumns: string[]) => {
    const [hiddenColumns, setHiddenColumns] = useState(initialHiddenColumns);

    const onChangeHiddenColumns = useCallback(
        (columns: string[]) => {
            setHiddenColumns(columns);

            userConfigSave(`v1--hidden-columns--${entityAlias}`, columns);
        },
        [entityAlias],
    );

    return {
        hiddenColumns,
        onChangeHiddenColumns,
    };
};
