import { NextPage } from "next";
import React, { useCallback } from "react";
import { container } from "../../config";
import {
    ForgottenPasswordForm,
    ForgottenPasswordFormData,
    ForgottenPasswordFormProps,
} from "../../forms/ForgottenPasswordForm";
import { LoginLayout } from "../../lib/LoginLayout";
import { ForgottenPasswordPageConfig } from "./types";

const INITIAL_VALUES: ForgottenPasswordFormData = {
    username: "",
};

export const ForgottenPasswordPage = (config: ForgottenPasswordPageConfig) => {
    const Component: NextPage = () => {
        const onSubmit = useCallback<ForgottenPasswordFormProps["onSubmit"]>(async (values) => {
            try {
                await config.onSubmit(values.username);
            } catch (e) {
                container.get("service.error").handleError(e);
            }
        }, []);

        return (
            <LoginLayout Logo={config.ui?.Logo} pageTitle={config.pageTitle} title={config.title}>
                <ForgottenPasswordForm onSubmit={onSubmit} initialValues={INITIAL_VALUES} onLogin={config.onLogin} />
            </LoginLayout>
        );
    };

    return Component;
};
