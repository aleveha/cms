import axios, { AxiosRequestConfig } from "axios";
import { createAxiosRequest } from "./createAxiosRequest";
import { createUseAxiosRequest } from "./createUseAxiosRequest";

export function createAxiosInstance(requestConfig: AxiosRequestConfig = {}) {
    const instance = axios.create();

    return {
        axiosRequest: createAxiosRequest(instance, requestConfig),
        useAxiosRequest: createUseAxiosRequest(instance, requestConfig),
    };
}
