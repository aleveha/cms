import { Schema } from "@uxf/data-grid";
import { EntityMetaSchema, dataGridSchemaGet, getEntityMetaSchemas } from "../../api";
import {
    gridSchemaExistsSelector,
    gridSchemaSelector,
    metaSchemaExistsSelector,
    metaSchemaSelector,
    onSetEntityMetaSchemas,
    onSetGridSchema,
} from "../../deprecated/redux/entities";

export async function loadEntityMetaSchema(ctx: any, entityAlias: string): Promise<EntityMetaSchema> {
    const {
        reduxStore: { getState, dispatch },
    } = ctx;

    if (!metaSchemaExistsSelector(getState())) {
        dispatch(onSetEntityMetaSchemas((await getEntityMetaSchemas(ctx)).data));
    }

    return metaSchemaSelector(entityAlias)(getState());
}

export async function loadEntityGridSchema(ctx: any, entityAlias: string): Promise<Schema<any>> {
    const {
        reduxStore: { getState, dispatch },
    } = ctx;

    if (!gridSchemaExistsSelector(entityAlias)(getState())) {
        dispatch(onSetGridSchema(entityAlias, (await dataGridSchemaGet(ctx, entityAlias)).data));
    }

    return gridSchemaSelector(entityAlias)(getState());
}
