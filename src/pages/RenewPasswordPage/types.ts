import { SubmissionErrors } from "final-form";
import { NextPageContext } from "next";
import { ReactNode } from "react";
import { RenewPasswordFormData } from "../../forms/RenewPasswordForm";

export interface RenewPasswordPageConfig {
    pageTitle?: string;
    title: ReactNode;
    checkToken?: (ctx: NextPageContext, token: string) => Promise<void>;
    onSubmit: (
        token: string,
        values: RenewPasswordFormData,
    ) => SubmissionErrors | Promise<SubmissionErrors | undefined> | undefined | void;
    ui?: {
        Logo?: ReactNode;
    };
}
