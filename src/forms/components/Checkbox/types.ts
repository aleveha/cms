import { ReactNode } from "react";

export interface CheckboxProps {
    id?: string;
    name: string;
    label?: ReactNode;
    helperText?: ReactNode;
    disabled?: boolean;
    required?: boolean;
}
