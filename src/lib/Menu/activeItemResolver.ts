import { Resolver } from "./types";

const exactMatch = (): Resolver => (menuItem) => (router) => {
    return router.route === menuItem.route;
};

const exactMatchWithParams = (): Resolver => (menuItem) => (router) => {
    const isQueryEqual = JSON.stringify(router.query) === JSON.stringify(menuItem.routeParams);
    return router.route === menuItem.route && isQueryEqual;
};

const anyOf = (routeNames: string[]): Resolver => (menuItem) => (router) => {
    if (router.route === menuItem.route) {
        return true;
    }

    return routeNames.some((routeName) => routeName === router.route);
};

const tableLink = (): Resolver => (menuItem) => (router) => {
    const isQueryEqual = router.query.entityAlias === menuItem.routeParams?.entityAlias;

    if (router.route === menuItem.route && isQueryEqual) {
        return true;
    }

    return router.route === "cms/table-form" && isQueryEqual;
};

export const ActiveItemResolver = {
    anyOf,
    exactMatch,
    exactMatchWithParams,
    tableLink,
};
