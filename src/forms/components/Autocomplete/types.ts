import { AutocompleteProps as UIAutocompleteProps } from "../../../ui/Autocomplete";
import { ReactNode } from "react";

interface ExtraAutocompleteProps<
    T,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
> {
    name: string;
    required?: boolean;
    requiredErrorMessage?: ReactNode;
    onChange?: UIAutocompleteProps<T, DisableClearable, FreeSolo>["onChange"];
}

export type AutocompleteProps<
    T,
    DisableClearable extends boolean | undefined = undefined,
    FreeSolo extends boolean | undefined = undefined,
> = Omit<UIAutocompleteProps<T, DisableClearable, FreeSolo>, "onChange" | "name" | "value" | "multiple"> &
    ExtraAutocompleteProps<T, DisableClearable, FreeSolo>;
