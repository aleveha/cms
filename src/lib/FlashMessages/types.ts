import { ReactNode } from "react";

export type FlashMessageVariant = "success" | "info" | "warning" | "error";

export interface FlashMessageConfig {
    id?: number;
    body?: ReactNode;
    dismissTimeout?: number;
    variant?: FlashMessageVariant;
    onClick?: () => void;
}

export interface FlashMessageProps {
    config: FlashMessageConfig;
    onClose: (config: FlashMessageConfig) => void;
    offset: number;
    closing?: boolean;
}

interface FlashMessageState {
    config: FlashMessageConfig;
    closing: boolean;
}

export interface FlashMessagesContainerState {
    items: FlashMessageState[];
}
