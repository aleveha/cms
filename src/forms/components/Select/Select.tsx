import { FieldValidator } from "final-form";
import React, { FocusEventHandler, ReactElement, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { Select as UISelect } from "../../../ui/Select";
import { SelectProps } from "./types";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<any> =>
    (value: string) =>
        value ? undefined : errorMessage;

export const Select = <
    T,
    Multiple extends boolean | undefined,
    DisableClearable extends boolean | undefined,
    FreeSolo extends boolean | undefined,
>(
    props: SelectProps<T, Multiple, DisableClearable, FreeSolo>,
): ReactElement => {
    const {
        label,
        name,
        onBlur,
        onChange,
        onFocus,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        TextFieldProps = {},
        ...selectProps
    } = props;

    const { input, meta } = useField<any>(name, {
        allowNull: true,
        defaultValue: null,
        validate: required ? requiredValidator(requiredErrorMessage) : undefined,
    });

    const onBlurHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onBlur(e);
            if (onBlur) {
                onBlur(e);
            }
        },
        [input, onBlur],
    );

    const onFocusHandler = useCallback<FocusEventHandler<HTMLInputElement>>(
        (e) => {
            input.onFocus(e);
            if (onFocus) {
                onFocus(e);
            }
        },
        [input, onFocus],
    );

    return (
        <UISelect
            {...selectProps}
            label={required ? `${label} *` : label}
            TextFieldProps={{
                ...TextFieldProps,
                helperText: meta.error && meta.touched ? meta.error : undefined,
                error: TextFieldProps.error || (meta.error && meta.touched),
            }}
            value={input.value}
            onChange={(event, value, reason, details) => {
                input.onChange(value);
                if (onChange) {
                    onChange(event, value, reason, details);
                }
            }}
            onBlur={onBlurHandler}
            onFocus={onFocusHandler}
        />
    );
};
