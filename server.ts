import express from "express";
import proxy from "express-http-proxy";
import next from "next";
import { getRequestHandler } from "./routes";

const app = next({ dev: process.env.NODE_ENV !== "production" });
const handler = getRequestHandler(app);

app.prepare().then(() => {
    express()
        .use(
            "/api",
            proxy(process.env.API_URL ?? "", { proxyReqPathResolver: (req: any) => "/api" + req.url, limit: "5mb" }),
        )
        .use(handler)
        .listen(3000, () => console.log("Listen on http://localhost:3000"));
});
