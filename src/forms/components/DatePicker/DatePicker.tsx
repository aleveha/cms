import { DatePicker as UIDatePicker, DatePickerProps as UIDatePickerProps } from "../../../ui/DatePicker";
import { FieldValidator } from "final-form";
import moment, { Moment } from "moment";
import React, { FC, ReactNode, useCallback } from "react";
import { useField } from "react-final-form";
import { composeValidators } from "../../../utils/composeValidators";
import { DatePickerProps } from "./types";

const requiredValidator =
    (errorMessage: ReactNode): FieldValidator<string> =>
    (value: string) =>
        value ? undefined : errorMessage;

export const DatePicker: FC<DatePickerProps> = (props) => {
    const {
        allowedFormats = ["l", "L"],
        disabled,
        error,
        name,
        required,
        requiredErrorMessage = "Vyplňte políčko",
        validate = [],
        ...restProps
    } = props;

    const validator = composeValidators(
        required && !disabled ? requiredValidator(requiredErrorMessage) : undefined,
        ...validate,
    );

    const { input, meta } = useField<string | null>(name, {
        allowNull: true,
        validate: validator,
    });

    const onChange = useCallback<UIDatePickerProps["onChange"]>(
        (date?: Moment | null) => {
            const d = !!date ? (date.isValid() ? date : moment(date.creationData().input, allowedFormats, true)) : null;
            input.onChange(d?.format("YYYY-MM-DD"));
        },
        [allowedFormats, input],
    );

    return (
        <UIDatePicker
            clearable
            showTodayButton
            value={moment(input.value) || null}
            onChange={onChange}
            disabled={disabled}
            required={required}
            error={error || (meta.touched && meta.error)}
            onBlur={input.onBlur}
            onFocus={input.onFocus}
            {...restProps}
        />
    );
};
