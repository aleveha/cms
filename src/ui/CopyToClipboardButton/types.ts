import { IconButtonTypeMap } from "@mui/material";

export interface ClipboardButtonProps {
    textToCopy: string;
    size?: IconButtonTypeMap["props"]["size"];
    color?: IconButtonTypeMap["props"]["color"];
}
