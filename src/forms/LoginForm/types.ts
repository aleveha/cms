import { FormProps } from "react-final-form";

export interface LoginFormData {
    username: string;
    password: string;
}

export interface LoginFormProps extends FormProps<LoginFormData> {
    onForgottenPassword?: () => void;
}
