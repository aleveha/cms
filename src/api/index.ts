import { Schema } from "@uxf/data-grid";
import { createAxiosInstance } from "../lib/api";

const { axiosRequest } = createAxiosInstance();

export interface FieldSchemaResponse {
    name: string;
    type: string;
    label: string;
    required: boolean;
    readOnly: boolean;
    editable: boolean;
    fields: FieldSchemaResponse[];
}

export interface FormSchemaResponse {
    fields: FieldSchemaResponse[];
}

export default interface UserResponse {
    id: number;
    email: string;
    name: string;
    roles: string[];
}

export interface UserInfoResponse {
    user: UserResponse;
}

export interface EntityMetaSchema {
    title: string;
    entityAlias: string;
    actions: string[];
}

export interface EntityMetaSchemasResponse {
    [entityAlias: string]: EntityMetaSchema;
}

export interface AutocompleteItemResponse {
    value: number;
    label: string;
}

export interface LoginRequestBody {
    username: string;
    password: string;
}

export interface LoginResponse {
    access_token: string;
    refresh_token: string;
    expires_in: number;
    token_type: string;
}

export interface UserConfigResponse<T extends any = any> {
    id: number;
    name: string;
    data: T;
}

export interface UserConfigBagResponse {
    [name: string]: UserConfigResponse;
}

export function dataGridSchemaGet(ctx: any, gridName: string) {
    return axiosRequest<Schema<any>>(ctx, `/api/cms/datagrid/schema/${gridName}`, "get", null, null);
}

export const dataGridAutocompleteGet = (ctx: any, gridName: string, filterName: string, text: string) => {
    return axiosRequest<Array<{ value: number; label: string }>>(
        ctx,
        `/api/cms/datagrid/autocomplete/${gridName}/${filterName}`,
        "get",
        null,
        { s: text },
    ).then((r) => r.data);
};

export function forgottenPassword(ctx: any, email: string, cms = true): Promise<void> {
    return axiosRequest(ctx, "/api/forgotten-password", "post", { email, cms }, null).then(() => undefined);
}

export function getLoggedUser(ctx: any = null) {
    return axiosRequest<UserInfoResponse>(ctx, `/api/cms/user`, "get", null, null).then((r) => r.data.user);
}

export function getEntityMetaSchemas(ctx: any) {
    return axiosRequest<EntityMetaSchemasResponse>(ctx, "/api/cms/tables", "get", null, null);
}

export function getFormSchema(ctx: any, path: { entityAlias: string }) {
    return axiosRequest<FormSchemaResponse>(ctx, `/api/cms/form/${path.entityAlias}/schema`, "get", null, null);
}

export function getFormValues(ctx: any, path: { entityAlias: string; id: number }) {
    return axiosRequest<any>(ctx, `/api/cms/form/${path.entityAlias}/${path.id}`, "get", null, null);
}

export function login(ctx: any, body: LoginRequestBody) {
    return axiosRequest<LoginResponse>(ctx, "/api/login", "post", body, null);
}

export function autocompleteRows(ctx: any, path: { entityAlias: string; fieldName: string }, query: { term: string }) {
    return axiosRequest<AutocompleteItemResponse[]>(
        ctx,
        `/api/cms/form/${path.entityAlias}/autocomplete/${path.fieldName}`,
        "get",
        null,
        query,
    );
}

export function saveFormValues<T extends {}>(ctx: any, path: { entityAlias: string; id?: number | null }, body: T) {
    return path.id
        ? axiosRequest<T>(ctx, `/api/cms/form/${path.entityAlias}/${path.id}`, "put", body, null)
        : axiosRequest<T>(ctx, `/api/cms/form/${path.entityAlias}`, "post", body, null);
}

export async function userConfigSave(name: string, data: any) {
    return axiosRequest<UserConfigBagResponse>(null, "/api/cms/user-config", "post", { name, data }, null);
}

export function userConfigGetAll(ctx: any) {
    return axiosRequest<UserConfigBagResponse>(ctx, "/api/cms/user-config", "get", null, null);
}

export async function userConfigGet<T extends any = any>(ctx: any, name: string, defaultValue?: T): Promise<T> {
    const configs = await userConfigGetAll(ctx).then((r) => r.data);
    return configs[name]?.data || defaultValue;
}
