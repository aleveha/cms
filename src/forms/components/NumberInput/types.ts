import { NumberInputProps as UINumberInputProps } from "../../../ui/NumberInput";
import { FieldValidator } from "final-form";
import { ReactNode } from "react";

export interface NumberInputProps extends Omit<UINumberInputProps, "name" | "onChange" | "value"> {
    name: string;
    validate?: FieldValidator<string>[];
    requiredErrorMessage?: ReactNode;
}
