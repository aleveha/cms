import ExtendableError from "es6-error";

export class NetworkError extends ExtendableError {
    constructor(message = "Network error.") {
        super(message);
    }
}
