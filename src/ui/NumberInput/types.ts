import { TextFieldProps } from "@mui/material";

export type NumberInputProps = TextFieldProps & {
    decimalPlaces?: number;
    value?: string;
    onChange?: (value: string, event: any) => void; // TODO event type
};
