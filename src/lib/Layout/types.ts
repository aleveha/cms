import { ReactNode } from "react";
import { MenuItem } from "../Menu/model";

export type LayoutProps = SidebarProps & {
    Breadcrumbs?: ReactNode;
    subtitle?: string;
    header?: ReactNode;
    Logo?: ReactNode;
    ToolbarRight?: ReactNode;
} & ({
    loading: true;
    title?: string;
} | {
    loading?: never;
    title: string;
})

export interface AppBarProps {
    Logo?: ReactNode;
    ToolbarRight?: ReactNode;
}

export interface SidebarProps {
    Menu?: ReactNode;
    UserMenu?: ReactNode;
    menuConfiguration?: MenuItem[];
}
