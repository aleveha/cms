build:
	./node_modules/.bin/tsc -P tsconfig.build.json

dev:
	./node_modules/.bin/ts-node -P tsconfig.server.json server.ts

lint:
	./node_modules/.bin/eslint -c .eslintrc.js "./**/*.ts*"

test: lint typecheck

typecheck:
	./node_modules/.bin/tsc --noEmit --skipLibCheck