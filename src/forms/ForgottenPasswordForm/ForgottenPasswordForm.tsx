import React, { useEffect, useState } from "react";
import { Button } from "../../ui/Button";
import { TextInput } from "../components/TextInput";
import { Form } from "react-final-form";
import { ForgottenPasswordFormProps } from "./types";

// TODO: email validation

export const ForgottenPasswordForm: React.FC<ForgottenPasswordFormProps> = (props) => {
    const { onLogin, ...formProps } = props;

    const [formDisabled, setFormDisabled] = useState(true);

    useEffect(() => {
        setFormDisabled(false);
    }, []);

    return (
        <Form {...formProps}>
            {({ handleSubmit, submitting }) => (
                <form onSubmit={handleSubmit}>
                    <TextInput disabled={formDisabled} label="E-mail" name="username" required type="text" />
                    <Button
                        color="primary"
                        disabled={submitting}
                        fullWidth
                        loading={submitting}
                        type="submit"
                        variant="contained"
                        mt={2}
                    >
                        Obnovit heslo
                    </Button>
                    {onLogin && (
                        <Button size="small" onClick={onLogin} disabled={submitting} fullWidth mt={2}>
                            Přihlásit
                        </Button>
                    )}
                </form>
            )}
        </Form>
    );
};
