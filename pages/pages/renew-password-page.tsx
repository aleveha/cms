import Avatar from "@mui/material/Avatar";
import { RenewPasswordPage } from "../../src/pages/RenewPasswordPage";

const Page = RenewPasswordPage({
    onSubmit: console.log,
    checkToken: async (_, token) => console.log(token),
    title: "UXFans",
    pageTitle: "UXFans | obnova hesla",
    ui: {
        Logo: <Avatar>U</Avatar>,
    },
});

export default Page;
