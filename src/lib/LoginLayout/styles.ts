import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";

export const LoginLayoutRoot = styled("main")(({ theme }) => ({
    alignItems: "center",
    display: "flex",
    justifyContent: "center",
    minHeight: "100vh",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingBottom: theme.spacing(10),
    paddingTop: theme.spacing(6),
    [theme.breakpoints.up("sm")]: {
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3),
    },
}));

export const LoginLayoutPaper = styled(Paper)(({ theme }) => ({
    alignItems: "center",
    display: "flex",
    flex: `0 1 ${theme.spacing(50)}`,
    flexDirection: "column",
    maxWidth: theme.spacing(50),
    paddingBottom: theme.spacing(3),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingTop: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3),
    },
}));

export const LoginLayoutHeader = styled("div")(({ theme }) => ({
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    marginBottom: theme.spacing(2),
}));
