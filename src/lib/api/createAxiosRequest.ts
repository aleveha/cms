import { AxiosInstance, AxiosRequestConfig, Method } from "axios";
import qs from "qs";
import { config } from "../../config";
import { BadRequestError } from "../../errors/BadRequestError";
import { ForbiddenError } from "../../errors/ForbiddenError";
import { NetworkError } from "../../errors/NetworkError";
import { UnauthorizedError } from "../../errors/UnauthorizedError";

export const createAxiosRequest = (axiosInstance: AxiosInstance, requestConfig: AxiosRequestConfig) => <
    Response extends any
>(
    ctx: any,
    url: string,
    method: Method,
    body: object | null,
    query: object | null,
) => {
    return axiosInstance
        .request<Response>({
            baseURL: config.get("api-url"),
            headers: ctx?.req?.headers.cookie ? { cookie: ctx.req.headers.cookie } : {},
            ...requestConfig,
            url: query && Object.keys(query).length > 0 ? `${url}?${qs.stringify(query)}` : url,
            method,
            data: body,
        })
        .catch((error) => {
            if (error.response) {
                if (error.response.status === 400) {
                    throw new BadRequestError(error.response.data);
                } else if (error.response.status === 401) {
                    throw new UnauthorizedError();
                } else if (error.response.status === 403) {
                    throw new ForbiddenError();
                }
            } else if (error.request) {
                throw new NetworkError();
            }

            throw error;
        });
};
