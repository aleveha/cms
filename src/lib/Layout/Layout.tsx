import { CircularProgress } from "@mui/material";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import MuiToolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Head from "next/head";
import React, { FC, useCallback } from "react";
import { config } from "../../config";
import { useCmsContext } from "../CmsProvider";
import { AppBar } from "./AppBar";
import { Sidebar } from "./Sidebar";
import { LayoutHeader, LayoutRoot } from "./styles";
import { LayoutProps } from "./types";

export const Layout: FC<LayoutProps> = (props) => {
    const {
        children,
        title = `Načítám\u2026`,
        Logo,
        Menu,
        menuConfiguration,
        UserMenu,
        header,
        Breadcrumbs,
        ToolbarRight,
        loading,
    } = props;

    const { close } = useCmsContext("sidebar");

    const onClick = useCallback(() => {
        if (window.innerWidth < 1280) {
            close();
        }
    }, [close]);

    return (
        <LayoutRoot onClick={onClick}>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta charSet="utf-8" />
                <title>
                    {title} | {config.get("app-name")}
                </title>
            </Head>
            <CssBaseline />
            <AppBar Logo={Logo} ToolbarRight={ToolbarRight} />
            <Sidebar menuConfiguration={menuConfiguration} Menu={Menu} UserMenu={UserMenu} />
            <Box component="main" sx={{ flexGrow: 1 }}>
                {loading ? (
                    <Box minHeight="60vh" display="flex" alignItems="center" justifyContent="center">
                        <Box textAlign="center">
                            <CircularProgress />
                            <Typography mt={1}>Načítám&hellip;</Typography>
                        </Box>
                    </Box>
                ) : (
                    <>
                        <MuiToolbar variant="dense" />
                        <LayoutHeader hasHeader={!!header} sx={{ p: 3 }}>
                            <div style={{ fontSize: 10 }}>{Breadcrumbs}</div>
                            <Typography variant="h4" component="h2" color={header ? "#fff" : undefined}>
                                {title}
                            </Typography>
                            {header}
                        </LayoutHeader>
                        <Box sx={{ padding: 3 }}>{children}</Box>
                    </>
                )}
            </Box>
        </LayoutRoot>
    );
};
